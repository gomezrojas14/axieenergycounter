package com.example.axieenergycounter


import android.renderscript.ScriptGroup
import androidx.databinding.BindingAdapter
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel


class MainViewModel: ViewModel() {
    // Energy Controller
    var totalEnergy = MutableLiveData("2/10")
    var xEnergy = 2

    // Rounds controller
    var totalRound = MutableLiveData("Ronda 1")
    var xRound = 1

    fun energyCount(energy: Int) {
        xEnergy += energy
        if (xEnergy <= 0) xEnergy = 0 else xEnergy
        if (xEnergy >= 10) xEnergy = 10 else xEnergy
        totalEnergy.value = "$xEnergy/10"
    }

    fun roundCount(round: Int) {
        xRound += round
        totalRound.value = "Ronda $xRound"
        energyCount(2)
    }

    fun newMatch() {
        xEnergy = 2
        xRound = 1
        totalEnergy.value = "$xEnergy/10"
        totalRound.value = "Ronda $xRound"
    }
}

