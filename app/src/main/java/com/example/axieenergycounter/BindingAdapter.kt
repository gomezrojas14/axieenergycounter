package com.example.axieenergycounter

import android.widget.TextView
import androidx.databinding.BindingAdapter


@BindingAdapter("mutableTextCheckX")
fun mutableTextCheckX(view: TextView, option: String?) {
    view.setOnClickListener{
        view.text = if (view.text == "X") "" else "X"
    }
}

@BindingAdapter("clearMutableTextCheckX")
fun clearMutableTextCheckX(view: TextView, refresh: Boolean) {
    if (view.text == "X" && refresh) view.text = ""
}